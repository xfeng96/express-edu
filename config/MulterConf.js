var multer = require("multer");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/root/express-edu/public');
    },
    filename: function (req, file, cb) {
        console.log(file);
        var fileFormat = (file.originalname).split(".");
        cb(null, req.body.courseId + '-' + req.body.partId + '.' + fileFormat[fileFormat.length - 1]);
    }
});

var upload = multer({storage : storage});

module.exports = upload;
