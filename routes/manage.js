var express = require('express');
var router = express.Router();
var mysql = require('mysql');
const path=require('path');
const fs=require('fs');
const multer=require('multer');

var connection = mysql.createConnection({
    host: '49.232.162.126',
    port: '3306',
    user: 'edu',
    password: 'edu',
    database: 'edu'
});
connection.connect();

router.get('/', function (req, res){
    res.render('manage');
})

router.get('/manageNAI', function (req, res){
    res.render('manageNAI');
});

router.post('/changeNAI', function (req,res){
    var changeData = {
        "id": req.body.id,
        "newCourseName": req.body.newCourseName,
        "newCourseInfo": req.body.newCourseInfo
    };
    var change = "UPDATE `edu`.`courseinfo` SET `coursename` = '" + changeData.newCourseName + "', `info` = '" + changeData.newCourseInfo + "' WHERE `id` = " + changeData.id;
    connection.query(change, (err, result) => {
        if (err) {
            res.send("err");
        } else {
            res.redirect('/home');
        }
    });
});

router.get('/managePDF', function (req, res){
    res.render('managePDF');
});

router.get('/manageVIDEO', function (req, res){
    res.render('manageVIDEO');
});

var upload = require("../config/MulterConf");
var uploadFileCtrl = require("../controller/uploadFileCtrl");

router.post("/changePDF", upload.single('file'), uploadFileCtrl.upload);

module.exports = router;